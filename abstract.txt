Robust robotic perception and manipulation of household objects
requires the ability to detect, localize and manipulate a wide variety
of objects, which may be highly reflective or transparent; for example
picking a metal fork out of a sink full of running water.  Existing
perceptual approaches based on photographs only take into account the
average intensity of light at each pixel, discarding information about
the angle from which that light approached, which limits their ability
to account for non-Lambertian objects and scenes.  To address this
problem, we demonstrate time-lapse light field photography with an
eye-in-hand camera of a manipulator robot.  An eye-in-hand robot can
capture both the intensity of rays, as in a conventional photograph,
as well as the direction of the rays.  We present a formal model for
robotic light-field photography that fits into a probabilistic
robotics framework.  We can perform 3D reconstruction with a monocular
camera by performing approximate maximum-likelihood estimates in the
model, corresponding to a software lens with a very wide baseline.
This information can be used to detect, localize and manipulate
non-Lambertian objects in non-Lambertian scenes: our approach enables
Baxter to pick a shiny metal fork out of a sink filled with running
water 24/25 times.  The techniques in this paper point the way toward
new approaches to robotic perception that leverage a robot's ability
to move its camera to infer the state of the external world.
